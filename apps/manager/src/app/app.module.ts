import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { SharedModule } from '@comment/shared';

import { AppRouting } from './app.routing';
import { AppComponent } from './app.component';

import { environment } from './../environments/environment';

@NgModule({
  imports: [
    BrowserModule,
    AppRouting,
    SharedModule.forRoot({
      baseApiUrl: environment.baseApiUrl
    })
  ],
  declarations: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule {}
