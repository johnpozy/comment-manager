import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostModule as FeaturePostModule } from '@comment/post';

import { PostRouting } from './post.routing';
import { PostComponent } from './post.component';
import { PostListComponent } from './post-list/post-list.component';
import { PostDetailComponent } from './post-detail/post-detail.component';

@NgModule({
  imports: [
    CommonModule,
    PostRouting,
    FeaturePostModule
  ],
  declarations: [PostComponent, PostListComponent, PostDetailComponent]
})
export class PostModule { }
