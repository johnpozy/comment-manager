import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'manager-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostListComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
