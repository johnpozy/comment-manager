import { Component } from '@angular/core';

@Component({
  selector: 'manager-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'manager';
}
