module.exports = {
  name: 'manager',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/manager',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
