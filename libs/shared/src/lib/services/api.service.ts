import { HttpClient, HttpHeaders, HttpResponse, HttpParams } from '@angular/common/http';
import { Injectable, Optional } from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export class ApiServiceConfig {
  baseApiUrl: String;
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private headers: HttpHeaders;

  private baseApiUrl;

  constructor(@Optional() config: ApiServiceConfig, private http: HttpClient) {
    if (config) {
      this.baseApiUrl = config.baseApiUrl;
    }
  }

  public get(path, params?: HttpParams): Observable<HttpResponse<any>> {
    return this.http
      .get(`${this.baseApiUrl}/${path}`, {
        headers: this.headers,
        observe: 'response',
        params: params
      })
      .pipe(map(response => response));
  }
}
