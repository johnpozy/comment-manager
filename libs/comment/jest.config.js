module.exports = {
  name: 'comment',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/comment',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js',
  ],
};
