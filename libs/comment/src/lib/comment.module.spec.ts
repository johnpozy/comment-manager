import { async, TestBed } from '@angular/core/testing';
import { CommentModule } from './comment.module';

describe('CommentModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommentModule],
    }).compileComponents();
  }));

  it('should create', () => {
    expect(CommentModule).toBeDefined();
  });
});
