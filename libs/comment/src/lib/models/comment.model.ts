export class Comment {
  public postId: number;

  public id: number;

  public name: string;

  public email: string;

  public body: string;

  constructor(comment: Comment) {
    this.postId = comment && comment.postId ? comment.postId : null;
    this.id = comment && comment.id ? comment.id : null;
    this.name = comment && comment.name ? comment.name : null;
    this.email = comment && comment.email ? comment.email : null;
    this.body = comment && comment.body ? comment.body : null;
  }
}
