import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { debounceTime, tap } from 'rxjs/operators';

@Component({
  selector: 'comment-comment-filter',
  templateUrl: './comment-filter.component.html',
  styleUrls: ['./comment-filter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CommentFilterComponent implements OnInit {
  public formFilter: FormGroup;

  @Output() valueChange: EventEmitter<any> = new EventEmitter(null);

  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this._initFormFiler();
  }

  private _initFormFiler = () => {
    this.formFilter = this._formBuilder.group({
      name: null,
      email: null,
      body: null
    });

    this.formFilter.valueChanges
      .pipe(
        debounceTime(500),
        tap(formValue => this.valueChange.emit(formValue))
      )
      .subscribe()
  }
}
