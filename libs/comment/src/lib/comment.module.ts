import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CommentListComponent } from './components/comment-list/comment-list.component';
import { CommentFilterComponent } from './components/comment-filter/comment-filter.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  declarations: [CommentListComponent, CommentFilterComponent],
  exports: [CommentListComponent, CommentFilterComponent],
})
export class CommentModule {}
