import { Injectable } from '@angular/core';

import { ApiService } from '@comment/shared';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Comment } from '../models/comment.model';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  constructor(private apiService: ApiService) {}

  public getByPostId(postId): Observable<Array<Comment>> {
    let params = new HttpParams();

    params = params.append('postId', postId);

    return this.apiService
      .get(`comments`, params)
      .pipe(map(response => response.body));
  }
}
