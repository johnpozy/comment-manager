import { User } from '@comment/user';
import { Comment } from '@comment/comment';

export class Post {
  public userId: number;

  public user: User;

  public id: number;

  public title: string;

  public body: string;

  public comments: Array<Comment>;

  constructor(post: Post) {
    this.userId = post && post.userId ? post.userId : null;
    this.user = post && post.user ? new User(post.user) : null;
    this.id = post && post.id ? post.id : null;
    this.title = post && post.title ? post.title : null;
    this.body = post && post.body ? post.body : null;
    this.comments = post && post.comments && post.comments.length > 0 ? post.comments.map(comment => new Comment(comment)) : [];
  }
}
