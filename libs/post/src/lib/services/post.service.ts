import { Injectable } from '@angular/core';

import { ApiService } from '@comment/shared';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Post } from '../models/post.model';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  constructor(private apiService: ApiService) {}

  public get(): Observable<Array<Post>> {
    return this.apiService
      .get(`posts`)
      .pipe(map(response => response.body));
  }

  public getById(postId): Observable<Post> {
    return this.apiService
      .get(`posts/${postId}`)
      .pipe(map(response => response.body));
  }
}
