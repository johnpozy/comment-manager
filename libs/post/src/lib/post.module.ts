import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { CommentModule as FeatureCommentModule } from '@comment/comment';

import { PostListComponent } from './components/post-list/post-list.component';
import { PostDetailComponent } from './components/post-detail/post-detail.component';

@NgModule({
  imports: [CommonModule, RouterModule, FeatureCommentModule],
  declarations: [PostListComponent, PostDetailComponent],
  exports: [PostListComponent, PostDetailComponent],
})
export class PostModule {}
