import { async, TestBed } from '@angular/core/testing';
import { PostModule } from './post.module';

describe('PostModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [PostModule],
    }).compileComponents();
  }));

  it('should create', () => {
    expect(PostModule).toBeDefined();
  });
});
