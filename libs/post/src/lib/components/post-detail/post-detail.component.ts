import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { UserService } from '@comment/user';
import { CommentService } from '@comment/comment';

import { Observable, of, forkJoin, throwError } from 'rxjs';
import { switchMap, map, tap, catchError } from 'rxjs/operators';

import { Post } from '../../models/post.model';
import { PostService } from '../../services/post.service';

@Component({
  selector: 'comment-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostDetailComponent implements OnInit {
  public post$: Observable<Post>;

  public isPostLoading$: Observable<boolean>;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _postService: PostService,
    private _commentService: CommentService,
    private _userService: UserService
  ) { }

  ngOnInit(): void {
    this._getPost();
  }

  public onCommentFilterValueChanged = value => {
    this._getPost(value);
  };

  private _getPost = (filterValue?) => {
    this.isPostLoading$ = of(true);

    this.post$ = this._activatedRoute.params
      .pipe(
        switchMap(params => {
          return forkJoin([
            this._postService.getById(params.postId),
            this._userService.get(),
            this._commentService.getByPostId(params.postId)
          ]);
        }),
        map(([post, users, comments]) => {
          users.forEach(user => {
            if (post.userId === user.id) {
              post.user = user;
            }
          });

          post.comments = comments;

          return new Post(post);
        }),
        map(post => {
          if (filterValue && filterValue.name) {
            post.comments = post.comments.filter(comment => comment.name.indexOf(filterValue.name) !== -1);
          }

          if (filterValue && filterValue.email) {
            post.comments = post.comments.filter(comment => comment.email.indexOf(filterValue.email) !== -1);
          }

          if (filterValue && filterValue.body) {
            post.comments = post.comments.filter(comment => comment.body.indexOf(filterValue.body) !== -1);
          }

          return post;
        }),
        tap(() => this.isPostLoading$ = of(false)),
        catchError(error => throwError(error))
      );
  };
}
