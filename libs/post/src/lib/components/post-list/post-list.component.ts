import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

import { UserService } from '@comment/user';

import { Observable, forkJoin, throwError, of } from 'rxjs';

import { PostService } from '../../services/post.service';
import { Post } from '../../models/post.model';
import { map, catchError, tap } from 'rxjs/operators';

@Component({
  selector: 'comment-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostListComponent implements OnInit {
  public posts$: Observable<Array<Post>>;

  public isPostsLoading$: Observable<boolean>;

  constructor(
    private _postService: PostService,
    private _userService: UserService,
  ) { }

  ngOnInit(): void {
    this._getPost();
  }

  private _getPost = () => {
    this.isPostsLoading$ = of(true);

    this.posts$ = forkJoin([
      this._postService.get(),
      this._userService.get()
    ])
    .pipe(
      map(([posts, users]) => {
        posts.forEach(post => {
          users.forEach(user => {
            if (post.userId === user.id) {
              post.user = user;
            }
          });
        });

        return posts.map(post => new Post(post));
      }),
      tap(() => this.isPostsLoading$ = of(false)),
      catchError(error => throwError(error))
    );
  };
}
