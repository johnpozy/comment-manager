class UserAddress {
  public street: string;

  public suite: string;

  public city: string;

  public zipcode: string;

  public geo: {
    lat: number,
    lng: number
  };

  constructor(address: UserAddress) {
    this.street = address && address.street ? address.street : null;
    this.suite = address && address.suite ? address.suite : null;
    this.city = address && address.city ? address.city : null;
    this.zipcode = address && address.zipcode ? address.zipcode : null;
    this.geo = address && address.geo ? address.geo : null;
  }
}

class UserCompany {
  public name: string;

  public catchPhrase: string;

  public bs: string;

  constructor(company: UserCompany) {
    this.name = company && company.name ? company.name : null;
    this.catchPhrase = company && company.catchPhrase ? company.catchPhrase : null;
    this.bs = company && company.bs ? company.bs : null;
  }
}

export class User {
  public id: number;

  public name: string;

  public username: string;

  public email: string;

  public address: UserAddress;

  public phone: string;

  public website: string;

  public company: UserCompany;

  constructor(user: User) {
    this.id = user && user.id ? user.id : null;
    this.name = user && user.name ? user.name : null;
    this.username = user && user.username ? user.username : null;
    this.email = user && user.email ? user.email : null;
    this.address = user && user.address ? new UserAddress(user.address) : null;
    this.phone = user && user.phone ? user.phone : null;
    this.website = user && user.website ? user.website : null;
    this.company = user && user.company ? new UserCompany(user.company) : null;
  }
}
