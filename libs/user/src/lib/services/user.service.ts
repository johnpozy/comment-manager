import { Injectable } from '@angular/core';

import { ApiService } from '@comment/shared';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private apiService: ApiService) {}

  public get(): Observable<Array<User>> {
    return this.apiService
      .get(`users`)
      .pipe(map(response => response.body));
  }
}
